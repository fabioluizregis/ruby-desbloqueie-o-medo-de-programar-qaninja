class AvengersHeadQuarters
    attr_accessor :list

    def initialize
        self.list = []
    end

    def put(avenger)
        self.list.push(avenger)
    end
end

#TDD - Test Data Driven
describe AvengersHeadQuarters do
    it 'deve adicionar um vingador' do
        hq = AvengersHeadQuarters.new

        hq.put('Spiderman')
        expect(hq.list).to include 'Spiderman'
    end

    it 'deve adicionar uma lista de vingadores' do
        hq = AvengersHeadQuarters.new

        hq.put('Thor')
        hq.put('Hulk')
        hq.put('Spiderman')

        resultado = hq.list.size > 0

        expect(hq.list.size).to be > 0
        expect(resultado).to be true
    end

    it 'thor deve ser o primeiro da lista' do
        hq = AvengersHeadQuarters.new

        hq.put('Thor')
        hq.put('Hulk')
        hq.put('Spiderman')

        expect(hq.list).to start_with('Thor')
    end

    it 'ironman deve ser o ultimo da lista' do
        hq = AvengersHeadQuarters.new

        hq.put('Thor')
        hq.put('Hulk')
        hq.put('Spiderman')
        hq.put('Ironman')

        expect(hq.list).to end_with('Ironman')
    end
    #Validando com expressão regular
    it 'deve contar o sobrenome' do
        avenger = 'Perter Parker'

        expect(avenger).to match(/Parker/)
        expect(avenger).not_to match(/Fabio/)
    end
end