# Ruby - Desbloqueie o medo de programar - QANinja

Exercícios do curso de Ruby - QANinja

execuções: 
- rspec .\spec\bank\
- rspec .\spec\bank\ -fd
- rspec .\spec\bank\ --format html --out report.html
- rspec .\spec\bank\ --format html --out report.html -fd
- rspec .\spec\bank\ --format RspecJunitFormatter --out report.xml
- rspec .\spec\bank\ --format RspecJunitFormatter --out report.xml -fd